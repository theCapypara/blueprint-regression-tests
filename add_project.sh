#!/bin/bash

shopt -s globstar

if [ "$#" -ne 3 ] ; then
    echo "usage: add_project.sh <dest directory> <url> <license>"
    exit 1
fi

mkdir tmp

git clone --depth=1 "$2" "tmp/$1"
mkdir "inputs/$1"
cp tmp/$1/**/*.blp "inputs/$1/"

echo "" >> README.md
echo "$1:" >> README.md
echo "<$2>" >> README.md
echo "$3" >> README.md

./update.sh

