#!/usr/bin/env bash

blueprint-compiler batch-compile outputs inputs $(find inputs -name *.blp)
diff -r outputs expected
