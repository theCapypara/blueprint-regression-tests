#!/usr/bin/env bash

for i in $(find inputs -name *.blp);
do
  file=${i#inputs/}
  mkdir -p "outputs/$(dirname $file)"
  blueprint-compiler compile --output "outputs/${file%.blp}.ui" "$i"
done
diff -r outputs expected
