using Gtk 4.0;
using Adw 1;

template OnboardWindow : Adw.ApplicationWindow {
  resizable: false;
  deletable: true;
  modal: true;
  default-width: 600;
  default-height: 550;

  Box {
    orientation: vertical;

    [titlebar]
    Adw.HeaderBar {
      title-widget: Adw.CarouselIndicatorDots {
        carousel: carousel;
        orientation: horizontal;
      };

      styles [
        "flat",
      ]

      [start]
      Button btn_prev {
        label: _("Previous");
        sensitive: false;
      }

      [end]
      Button btn_next {
        label: _("Next");
        sensitive: true;

        styles [
          "suggested-action",
        ]
      }
    }

    Adw.Carousel carousel {
      vexpand: true;
      hexpand: true;
      allow-mouse-drag: true;
      allow-long-swipes: false;

      Adw.StatusPage page_welcome {
        icon-name: "dev.bscubed.Gelata";
        title: _("Welcome to Gelata!");
        description: _("A simple and clean Jellyfin client for the modern desktop");
        vexpand: true;
        hexpand: true;
      }

      Box page_authenticate {
        orientation: vertical;

        Adw.StatusPage {
          title: _("Let\'s get started");
          description: _("Enter your Jellyfin server information");
          vexpand: true;
          hexpand: true;
        }

        Adw.PreferencesPage {
          Adw.PreferencesGroup {
            Adw.ActionRow {
              title: _("Server Address");
              focusable: false;

              Entry server {
                placeholder-text: "127.0.0.1";
                activates-default: true;
                width-request: 250;
                valign: center;
              }
            }

            Adw.ActionRow {
              title: _("Username");
              focusable: false;

              Entry username {
                activates-default: true;
                width-request: 250;
                valign: center;
              }
            }

            Adw.ActionRow {
              title: _("Password");
              focusable: false;

              PasswordEntry password {
                show-peek-icon: true;
                activates-default: true;
                width-request: 250;
                valign: center;
              }
            }
          }

          Adw.PreferencesGroup {
            Adw.ExpanderRow {
              title: _("Advanced Options");

              Adw.ActionRow {
                title: _("Port");
                subtitle: _("Custom port other than the default.");
                focusable: false;

                Entry port {
                  placeholder-text: "8096";
                  activates-default: true;
                  width-request: 250;
                  valign: center;
                }
              }

              Adw.ActionRow {
                activatable: true;
                title: _("Use HTTPS");
                subtitle: _("Enforces connection over HTTPS.");

                Switch use_https {
                  valign: center;
                  halign: end;
                }
              }
            }
          }
        }
      }

      Adw.StatusPage page_finish {
        icon-name: "emoji-body-symbolic";
        title: _("All ready!");
        vexpand: true;
        hexpand: true;
        valign: center;
        child: Button btn_close {
          label: _("Start using Gelata");
          use-underline: true;
          halign: center;

          styles [
            "suggested-action",
            "pill",
          ]
        };
      }
    }
  }
}
