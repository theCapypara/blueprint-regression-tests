using Gtk 4.0;
using Adw 1;

template CPageDetails : Gtk.Box {
	orientation: vertical;
	hexpand: true;
	vexpand: true;
	width-request: 360;

	Adw.HeaderBar headerbar {

		title-widget: Adw.WindowTitle windowtitle {
			title: bind CPageDetails.title;
			subtitle: bind CPageDetails.authors;
		};

		[start]
		Gtk.Button {
			icon-name: "go-previous-symbolic";
			action-name: 'win.subpage-back';
		}

		[start]
		Gtk.Button {
			icon-name: "system-search-symbolic";
		}

		[end]
		Gtk.Button {
			icon-name: "open-menu-symbolic";
		}
	}

	Gtk.ScrolledWindow {
		hexpand: true;
		vexpand: true;

		Adw.Clamp {
			Gtk.Box pagebox {
				styles ['main-box']
				orientation: vertical;
				spacing: 24;

				Gtk.Box {
					orientation: vertical;
					spacing: 6;
					Gtk.Label {
						styles ['title-1']
						label: bind CPageDetails.title;
						wrap: true;
						halign: start;
					}
					Gtk.Label {
						label: bind CPageDetails.subtitle;
						visible: bind CPageDetails.has-subtitle;
						wrap: true;
						halign: start;
					}
					Gtk.Label {
						styles ['dim-label']
						label: bind CPageDetails.authors;
						visible: bind CPageDetails.has-authors;
						wrap: true;
						halign: start;
					}
				}

				Gtk.Box {
					spacing: 6;
					Gtk.Box {
						hexpand: true;
						orientation: vertical;
						spacing: 6;

						Gtk.Label {
							styles ['caption-heading']
							halign: start;
							label: bind CPageDetails.time;
						}
						Gtk.Label {
							styles ['caption']
							halign: start;
							wrap: true;
							label: bind CPageDetails.room;
						}
						Gtk.Label {
							styles ['caption']
							halign: start;
							wrap: true;
							label: bind CPageDetails.track;
						}
					}

					.CStarButton star_button {
						clicked => toggle_starred();
					}
				}


				Gtk.Box {
					orientation: vertical;
					spacing: 12;
					visible: bind conflicts_list.visible;
					Gtk.Label {
						styles ['heading']
						halign: start;
						label: _("Conflicts with");
					}
					Gtk.ListBox conflicts_list {
						styles ['boxed-list']
						row-activated => on_conflict_activated();
					}
				}

				Gtk.Label {
					halign: start;
					wrap: true;
					label: bind CPageDetails.abstract;
					visible: bind CPageDetails.has-abstract;
				}

				Gtk.Label {
					halign: start;
					wrap: true;
					label: bind CPageDetails.description;
					visible: bind CPageDetails.has-description;
				}

				Gtk.Box {
					orientation: vertical;
					spacing: 12;
					visible: bind links_list.visible;
					Gtk.Label {
						styles ['heading']
						halign: start;
						label: _("Links");
					}
					Gtk.ListBox links_list {
						styles ['boxed-list']
						row-activated => on_link_activated();
					}
				}

			}
		}
	}
}