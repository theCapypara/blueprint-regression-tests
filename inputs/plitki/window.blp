using Gtk 4.0;
using Adw 1;

template PlitkiWindow : Adw.ApplicationWindow {
  title: "Plitki";

  default-width: 1280;
  default-height: 720;

  content: Adw.ToastOverlay toast_overlay {
    child: Stack stack {
      transition-type: crossfade;

      StackPage {
        child: Box {
          orientation: vertical;

          Adw.HeaderBar {
            [start]
            Button {
              label: "Open";
              
              styles ["suggested-action"]

              clicked => on_open_clicked() swapped;
            }

            title-widget: Adw.WindowTitle {
                title: bind PlitkiWindow.title;
            };

            [end]
            Button {
              icon-name: "document-properties-symbolic";
              clicked => open_preferences() swapped;
            }
          }

          Adw.StatusPage {
            vexpand: true;
            description: "Select a .qua map by clicking the Open button.";
          }
        };
      }

      StackPage {
        name: "gameplay";

        child: Box {
          orientation: vertical;

          Adw.HeaderBar {
            title-widget: Adw.WindowTitle gameplay_window_title {};

            [end]
            Button {
              can-focus: false; // Don't steal spacebar from the playfield...
              icon-name: "document-properties-symbolic";
              clicked => open_preferences() swapped;
            }
          }

          Overlay {
            vexpand: true;

            child: .PlitkiBackground map_background {
              dim: bind background_dim_adjustment.value;
            };

            [overlay]
            Overlay {
              halign: center;

              layout {
                measure: true;
              }

              child: .PlitkiPlayfield playfield {
                downscroll: true;

                scroll-speed: bind scroll_speed_adjustment.value;
                lane-width: bind lane_width_adjustment.value;
                hit-position: bind hit_position_adjustment.value;
              };

              [overlay]
              Adw.Bin {
                valign: end;
                height-request: bind playfield.hit-position;

                child: Separator {
                  styles ["hit-position"]
                  valign: start;
                };

                layout {
                  clip-overlay: true;
                }
              }
            }

            [overlay]
            .PlitkiCombo combo {
              halign: start;
              valign: end;

              layout {
                measure: true;
              }
            }

            [overlay]
            .PlitkiAccuracy accuracy {
              halign: end;
              valign: start;

              layout {
                measure: true;
              }
            }

            [overlay]
            .PlitkiHitError hit_error {
              halign: center;
              valign: center;

              layout {
                measure: true;
              }
            }

            [overlay]
            .PlitkiJudgement judgement {
              halign: center;
              valign: end;

              layout {
                measure: true;
              }
            }
          }
        };
      }
    };
  };
}

Adw.PreferencesWindow pref_window {
  modal: false;
  hide-on-close: true;
  
  Adw.PreferencesPage {
    Adw.PreferencesGroup {
      title: "Playfield";

      Adw.ActionRow {
        title: "Lane Width";

        SpinButton {
          valign: center;
          
          adjustment: Adjustment lane_width_adjustment {
            lower: 0;
            upper: 1000;
            step-increment: 10;
            page-increment: 50;
            value: 110;
          };
        }
      }

      Adw.ActionRow {
        title: "Hit Position";

        SpinButton {
          valign: center;

          adjustment: Adjustment hit_position_adjustment {
            lower: 0;
            upper: 1000;
            step-increment: 10;
            page-increment: 50;
            value: 150;
          };
        }
      }

      Adw.ActionRow {
        title: "Scroll Speed";

        SpinButton {
          valign: center;

          adjustment: Adjustment scroll_speed_adjustment {
            lower: 1;
            upper: 255;
            step-increment: 5;
            page-increment: 10;
            value: 49;
          };
        }
      }
    }

    Adw.PreferencesGroup {
      title: "Stage";

      Adw.ActionRow {
        title: "Background Dim";

        Scale {
          hexpand: true;

          adjustment: Adjustment background_dim_adjustment {
            lower: 0;
            upper: 1;
            step-increment: 0.1;
            page-increment: 0.5;
            value: 0.9;
          };
        }
      }
    }

    Adw.PreferencesGroup {
      title: "Audio";

      Adw.ActionRow {
        title: "Device Offset";
        subtitle: "Global audio offset in milliseconds";

        SpinButton {
          valign: center;

          adjustment: Adjustment global_offset_adjustment {
            lower: -300;
            upper: 300;
            step-increment: 1;
            page-increment: 5;
            value: -40;

            value-changed => on_global_offset_changed() swapped;
          };
        }
      }
    }
  }
}